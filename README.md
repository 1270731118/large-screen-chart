# 大屏自适应图表

    技术栈:VUE3（3.2+）+ Vite(^2.9.0) + echarts（^5.3.0）

## 示例

[查看示例在线地址：](https://www.ruiazernia.com.cn/)


[查看使用案例 large_screen_demo分支](https://gitee.com/LBWlbw/large-screen-chart/tree/large_screen_demo/)

克隆分支：`git clone -b large_screen_demo https://gitee.com/LBWlbw/large-screen-chart.git`

## 使用方法

- **安装**
```bash
cnpm/npm  install --save largescreenchart;
```

- **使用**
```javascript
// @main.js

import {ChartPanel, screenAdapter } from 'largescreenchart'; 

app.use(ChartPanel).use(screenAdapter);

import 'largescreenchart/dist/style.css'

// @app

<screenAdapter w='19220' h='1080'>
    <ChartPanel width='xxxx' height='xxxx' :chartOption='chartOption' />
</screenAdapter>
```

- **支持自定义组件导入**
```javascript
// @main.js
import largescreenchart from 'largescreenchart'; 
app.use(largescreenchart);
    
// @app
<largescreenchart.screenAdapter w='19220' h='1080'>
    <largescreenchart.ChartPanel width='xxxx' height='xxxx' :chartOption='chartOption' />
</largescreenchart.screenAdapter>
```


## Props

## screenAdapter  设计稿尺寸

```javascript
    //设计稿尺寸宽度
       w: {
            type: Number,
            default: 1920
        },
    //设计稿尺寸高度
        h: {
            type: Number,
            default: 1080
        }
```

## ChartPanel

```javascript
//图表宽度
width: {
    type: String,
    default: '100%'
},
//图表高度
height: {
    type: String,
    default: '350px'
},
//渲染图表的options
chartOption: {
    type: Object,
    required: true
}
```
