import { createApp } from 'vue'
import App from './App.vue'


import eChartFn from './packages/importCharts.js'


import largeScreenChart from '../dist/largescreenchart.es.js';
let {  screenAdapter, ChartPanel} = largeScreenChart

import 'largescreenchart/dist/style.css'

const app = createApp(App)

app.config.globalProperties.$eChartFn = eChartFn

app
.use(screenAdapter)
.use(ChartPanel)
.mount('#app')
