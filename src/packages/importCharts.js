const modulesFiles = import.meta.globEager('./options/**/*.js');
let modules = {}
Object.keys(modulesFiles).forEach(item => {
    let mod = modulesFiles[item]
    const file = mod.default;
  modules = Object.assign({}, modules, file)
})
export default modules


