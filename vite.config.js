import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import OptimizationPersist from 'vite-plugin-optimize-persist'
import PkgConfig from 'vite-plugin-package-config'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    PkgConfig(),
    OptimizationPersist(),
    vue()
  ],
  build: {
    rollupOptions: {
      // 请确保外部化那些你的库中不需要的依赖
      external: ['vue'],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue',
        },
      },
    },
    lib: {
      entry: './src/packages/index.js',
      name: 'largeScreenChart',
    },
  },
})
